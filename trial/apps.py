# coding=utf-8
from django.apps import AppConfig


class TrialConfig(AppConfig):
    name = 'trial'
