# coding=utf-8
from django.urls import path

from rest_framework.urlpatterns import format_suffix_patterns

from trial import views


urlpatterns = [
    path('', views.api_root, name='root'),
    path('pages/', views.PageList.as_view(), name='pages-list'),
    path('pages/<int:pk>/', views.PageDetail.as_view(), name='pages-detail')
]


urlpatterns = format_suffix_patterns(urlpatterns)
