# coding=utf-8
"""Abstract models are here for circular import breaking."""

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models


class Titled(models.Model):
    """Abstract objects having a text title."""

    title = models.CharField(max_length=200, db_index=True)

    class Meta:
        abstract = True


class PageContentCommon(Titled):
    """
    Abstract object defining common properties of a page content element.

    Usage:
        Inherit your new element from this class and decorate with @export to
        make it visible outside.
    """

    counter = models.PositiveIntegerField(default=0)
    page_content = GenericRelation('PageContent', object_id_field='content_id')

    class Meta:
        abstract = True
