# coding=utf-8
import random
import logging

from django.db import migrations, IntegrityError
from django.conf import settings

from ..models import Page, PageContent
from ..models_abstract import PageContentCommon
from ..models_content import *

from ..util import RandomWords


# This module logger
mod_logger = logging.getLogger()


def create_random_content(content_class: PageContentCommon):
    """
    Create the instance of a content class object, fill with random data
    and save to database.

    :param content_class: a class (not instance or str!),
        inheriting PageContentCommon
    :return: instance of this class
    """

    rn_words = RandomWords()
    try:
        # _meta is undocumented, but it's usage seems acceptable:
        # https://www.b-list.org/weblog/2007/nov/04/working-models/
        max_title_len = content_class._meta.get_field('title').max_length
    except AttributeError:
        mod_logger.error('Content class without title field or max_length')
        return

    # Build and crop title string
    assert max_title_len > 0
    title = rn_words.build_phrase(4)[:max_title_len]

    # Create and fill
    if content_class is Video:
        obj = Video(
            title=title,
            video_url=rn_words.build_href(),
            subtitle_url=rn_words.build_href()
        )

    elif content_class is Audio:
        obj = Audio(
            title=title,
            bitrate=random.randint(1, 1024 ** 2)
        )

    elif content_class is Text:
        obj = Text(
            title=title,
            text=rn_words.build_phrase(50)
        )

    else:
        raise TypeError('Unknown content class')

    obj.save()
    return obj


def fill_random_data(num_pages, num_content_entries, num_links_range):
    """
    Fill database with random data (model-wise).

    :param num_pages: number of Page objects to create
    :param num_content_entries: number of PageContentCommon ancestors to create
    :param num_links_range: 2-element tuple (min, max) number of links between
        random pages and contents to establish
    """
    # Could use more RAM-efficient algorithms for this, like:
    # .objects.values_list('id', flat=True)
    # but preferred not to bang my head over this too much
    contents = []

    # Create some content
    choices = PageContentCommon.__subclasses__()
    for i in range(num_content_entries):
        # Create record of random type filled with random data
        content_class = random.choice(choices)
        content = create_random_content(content_class)
        content.save()
        contents.append(content)

    # Create pages
    for i in range(num_pages):
        page = Page(title=f'Page {i}')
        page.save()

        # Assign random contents and position
        for _ in range(random.randint(*num_links_range)):
            try:
                content = random.choice(contents)
                link = PageContent(
                    page=page,
                    # TODO: get limits from field validators
                    order_idx=random.randint(-2147483648, 2147483647),
                    content_obj=content
                )
                link.save()
            except IntegrityError:
                mod_logger.warning('Uniqueness constraint hit')


def create_demo_objects(apps, schema_editor):
    if settings.TEST_MODE:
        mod_logger.info('Skipping this migration for testing...')
        return

    num_pages = 49
    num_content_entries = 100

    # Min/max content links for page
    num_links_range = (0, 15)

    return fill_random_data(num_pages, num_content_entries, num_links_range)


class Migration(migrations.Migration):
    # Warning: since this migration is a...hm...migration, it will be applied
    # to a test database as well!
    # FIXME: exclude it somehow for speed

    dependencies = [
        ('trial', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(
            create_demo_objects,
            reverse_code=migrations.RunPython.noop
        ),
    ]
