# Generated by Django 2.1.5 on 2019-02-01 00:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('auth', '__latest__')
    ]

    operations = [
        migrations.CreateModel(
            name='Audio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(db_index=True, max_length=200)),
                ('counter', models.PositiveIntegerField(default=0)),
                ('bitrate', models.PositiveIntegerField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(db_index=True, max_length=200)),
            ],
            options={
                'ordering': ('pk',),
            },
        ),
        migrations.CreateModel(
            name='PageContent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content_id', models.PositiveIntegerField()),
                ('order_idx', models.IntegerField(default=0)),
                ('content_type', models.ForeignKey(limit_choices_to=models.Q(('app_label', 'trial'), ('model__in', ['video', 'audio', 'text'])), on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType')),
                ('page', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='contents', to='trial.Page')),
            ],
            options={
                'ordering': ('order_idx',),
            },
        ),
        migrations.CreateModel(
            name='Text',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(db_index=True, max_length=200)),
                ('counter', models.PositiveIntegerField(default=0)),
                ('text', models.TextField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(db_index=True, max_length=200)),
                ('counter', models.PositiveIntegerField(default=0)),
                ('video_url', models.URLField()),
                ('subtitle_url', models.URLField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AlterUniqueTogether(
            name='pagecontent',
            unique_together={('page', 'order_idx')},
        ),
    ]
