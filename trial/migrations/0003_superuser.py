# coding=utf-8
from django.contrib.auth.models import User
from django.db import migrations


def forward(apps, schema_editor):
    if User.objects.filter(is_superuser=True).count() == 0:
        User.objects.create_superuser(
            'admin', email='demo@localhost', password='admin')


class Migration(migrations.Migration):

    dependencies = [
        ('trial', '0002_sample_data')
    ]

    operations = [
        migrations.RunPython(
            forward,
            reverse_code=migrations.RunPython.noop
        )
    ]
