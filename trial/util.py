# coding=utf-8
import sys
import random
import logging


def export(obj):
    """
    Decorator for adding `obj` into __all__.
    Also creates module-wide exports_list variable, which lists
    exported classes as objects.
    """

    mod = sys.modules[obj.__module__]
    if hasattr(mod, '__all__'):
        mod.__all__.append(obj.__name__)
    else:
        mod.__all__ = [obj.__name__]

    if hasattr(mod, 'exports_list'):
        mod.exports_list.append(obj)
    else:
        mod.exports_list = [obj]

    return obj


class RandomWords(object):
    """
    This class can generate random words, phrases and URLs (with path).

    It tries to fetch words from `/usr/share/dict/words` first (on Linux),
    then, if unsuccessful, tries downloading from the web. Any exception
    raised at this stage is unhandled.

    Note: it is a singleton, wordlist is built only once.
    """
    def __init__(self):
        # FIXME: ugly
        if hasattr(self, 'words'):
            return

        self.words = None
        self.logger = logging.getLogger()
        self.logger.debug('Creating RandomWords...')
        word_file = '/usr/share/dict/words'
        try:
            self.words = open(word_file).read().splitlines()
        except FileNotFoundError:
            self.logger.warning(
                'No words file found at %s, trying to download...', word_file)

        # If this throws as well, program cannot continue
        if not self.words:
            import requests

            word_site = 'http://svnweb.freebsd.org/csrg/share/dict/words?view=co&content-type=text/plain'
            response = requests.get(word_site)
            self.words = [
                x.decode('utf-8')
                for x in response.content.splitlines()
            ]
            self.logger.info('Words file downloaded')

        assert self.words

    # Singleton stuff
    __instance = None

    def __new__(cls, *args, **kwargs):
        if cls.__instance is None:
            cls.__instance = super(RandomWords, cls).__new__(cls, *args, **kwargs)
        return cls.__instance

    def build_phrase(self, numwords) -> str:
        """
        Build random phrase (without punctuation).

        :param numwords: number of words to append
        :return: random phrase
        """

        return ' '.join(
             random.choice(self.words)
             for _ in range(numwords)
        )

    def build_href(self, max_domains: int = 3, max_path: int = 3) -> str:
        """
        Build random URL.
        Will generate random URL with domain level up to `2 + max_domains`
        and path with depth up to `max_path`.

        Domain names are lowercase, path names can be any case.

        :param max_domains: maximum domain level after required 2
            (max_domains == 1 will generate 3-rd level domain name)
        :param max_path: maximum path depth (0 = no path)
        :return: string with random URL
        """
        assert max_domains >= 0
        assert max_path >= 0

        schema = random.choice(['http', 'https'])
        dom = random.choice(['com', 'net', 'im', 'org', 'local'])

        base = '.'.join(
            random.choice(self.words)
            for _ in range(random.randint(1, max_domains + 1))
        ).lower()

        path = '/'.join(
            random.choice(self.words)
            for _ in range(random.randint(0, max_path))
        )

        return f'{schema}://{base}.{dom}{"/" if path else ""}{path}'

    def one_word(self) -> str:
        """
        Generate 1 random word.

        :return: string with random word
        """
        return random.choice(self.words)
