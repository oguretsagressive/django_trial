# coding=utf-8
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse

from .models import Page
from .serializers import PageListSerializer, PageDetailSerializer


@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'pages': reverse('pages-list', request=request, format=format)
    })


class PageList(generics.ListCreateAPIView):
    queryset = Page.objects.all()
    serializer_class = PageListSerializer


class PageDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Page.objects.all()
    serializer_class = PageDetailSerializer
