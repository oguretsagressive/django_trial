# coding=utf-8
from django.contrib import admin

from .models import Page, PageContent
from .models_content import exports_list as content_exports


class TitledAdmin(admin.ModelAdmin):
    list_display = ('title',)

    # FIXME: splitting interferes with `startswith` prefix:
    # https://code.djangoproject.com/ticket/6933
    search_fields = ['^title']

    def get_search_results(self, request, queryset, search_term):
        use_distinct = False
        if search_term:
            queryset &= self.model.objects.filter(title__istartswith=search_term)

        return queryset, use_distinct


@admin.register(*content_exports)
class ContentAdmin(TitledAdmin):
    pass


class ContentInline(admin.StackedInline):
    model = PageContent
    extra = 0

    # For all fields
    fields = None


class PageAdmin(TitledAdmin):
    fieldsets = [
        (None, {'fields': ['title']}),
    ]

    inlines = [ContentInline]


# FIXME: inconvenient, possible solution:
# https://github.com/arthanson/django-genericadmin
class PageContentAdmin(admin.ModelAdmin):
    model = PageContent

    # For all fields
    fields = None


admin.site.register(Page, PageAdmin)
admin.site.register(PageContent, PageContentAdmin)
