# coding=utf-8
import logging

from trial.celery import app
from django.db import models

import trial.models_content

logger = logging.getLogger()


@app.task
def task_content_hit_counter(pks_to_update):
    """
    Increment hit counter on objects referenced by pks_to_update list.

    :param content_obj: list of tuples (ORM object name, pk)
    """

    for class_name, pk in pks_to_update:
        try:
            obj = trial.models_content.__dict__[class_name]
            # TODO: ensure it doesn't fetch unnecessary data
            obj.objects.filter(pk=pk).update(counter=models.F('counter') + 1)
        except KeyError:
            logger.error(f'Class {class_name} not found in content models')
