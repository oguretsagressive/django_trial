# coding=utf-8
from django.urls import reverse
from django.db import IntegrityError

from rest_framework import status
from rest_framework.test import APITestCase

from .models import Page, PageContent
from .models_content import *

from django.test.utils import override_settings
from django.test.runner import DiscoverRunner
from django.conf import settings


class ExplicitTestSuiteRunner(DiscoverRunner):
    def __init__(self, *args, **kwargs):
        # Mark mode for skipping certain migrations
        settings.TEST_MODE = True

        # Reconfigure Celery for testing
        settings.CELERY_EAGER_PROPAGATES_EXCEPTIONS = True,
        settings.CELERY_ALWAYS_EAGER = True,
        settings.BROKER_BACKEND = 'memory'

        super(ExplicitTestSuiteRunner, self).__init__(*args, **kwargs)


class PageListTests(APITestCase):
    # Testing ListCreateAPIView

    def test_create_page(self):
        url = reverse('pages-list')
        data = {'title': 'Created with API'}
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Page.objects.count(), 1)
        self.assertEqual(Page.objects.get().title, 'Created with API')

    def test_list_pages(self):
        url = reverse('pages-list')

        # Create 15 pages
        for i in range(15):
            Page.objects.create(title=f'Page {i}')

        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Data retrieval test
        data = response.data
        self.assertGreater(data['count'], 10)

        # Pagination test
        self.assertEqual(len(response.data['results']), 10)


class PageDetailTests(APITestCase):
    # Testing RetrieveUpdateDestroyAPIView

    @classmethod
    def setUpTestData(cls):
        """
        Create common test data.

        Including: 1 page and 3 different content objects attached to it.
        """

        page = Page.objects.create(title='Page Test')
        cls.page_pk = page.pk
        contents = [
            Video.objects.create(
                title='Video Test',
                video_url='http://test.com/video.mp4',
                subtitle_url='http://test.com/video.srt'
            ),
            Audio.objects.create(title='Audio Test', bitrate=1024),
            Text.objects.create(title='Text test', text='Sample text')
        ]

        # Link objects
        for idx, content in enumerate(contents):
            PageContent.objects.create(
                page=page, content_obj=content, order_idx=idx)

    def test_retrieve_page(self):
        pk = self.page_pk

        url = reverse('pages-detail', kwargs={'pk': pk})
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data

        self.assertGreater(len(data['title']), 0)
        self.assertEqual(len(data['contents']), 3)
        self.assertIn('order_idx', data['contents'][0])
        self.assertIn('content_obj', data['contents'][0])

    def test_update_page(self):
        pk = self.page_pk

        url = reverse('pages-detail', kwargs={'pk': pk})
        new_title = 'Changed title 187f82ab'
        data = {'title': new_title}
        response = self.client.patch(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Page.objects.get(pk=pk).title, new_title)

    def test_delete_page(self):
        pk = self.page_pk

        url = reverse('pages-detail', kwargs={'pk': pk})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Page.objects.filter(pk=pk).count(), 0)

    def test_hit_counter(self):

        def get_counter_values(data):
            """Gather counter values from data."""
            return {
                x['id']: x['content_obj']['counter']
                for x in data['contents']
            }

        pk = self.page_pk
        url = reverse('pages-detail', kwargs={'pk': pk})

        # Perform initial retrieve
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        last_cnt = get_counter_values(response.data)

        # Repeat retrieval
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        current_cnt = get_counter_values(response.data)

        # Compare
        self.assertEqual(len(last_cnt), len(current_cnt))
        self.assertTrue(all(
            (last_cnt[k] + 1) == v
            for k, v in current_cnt.items()
        ))

    def test_order_collision(self):
        pc = PageContent.objects.filter(page__pk=self.page_pk).all()
        a, b = pc[:2]
        with self.assertRaises(IntegrityError):
            a.order_idx = b.order_idx
            a.save()
