# coding=utf-8
"""
List your custom content models here.

Inherit all of them from `PageContentCommon` and use @export for
final versions.
"""

from django.db import models

from .models_abstract import PageContentCommon
from .util import export


@export
class Video(PageContentCommon):
    video_url = models.URLField()
    subtitle_url = models.URLField()


@export
class Audio(PageContentCommon):
    bitrate = models.PositiveIntegerField()


@export
class Text(PageContentCommon):
    text = models.TextField()
