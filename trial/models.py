# coding=utf-8
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models

from .models_abstract import Titled
from .models_content import exports_list as content_classes
from .util import export

__all__ = []


@export
class Page(Titled):

    class Meta:
        # This will satisfy Django's paginator
        # TODO: ordering by `title` raises coupling?
        ordering = ('pk',)


@export
class PageContent(models.Model):
    """
    Intermediate entity for linking pages and content in an orderly manner.
    """

    page = models.ForeignKey(
        Page, on_delete=models.CASCADE, related_name='contents')

    content_type = models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE,
        limit_choices_to=models.Q(
            app_label='trial',
            # FIXME: depending on `_meta` internal
            model__in=[cls._meta.model_name for cls in content_classes]
        )
    )

    content_id = models.PositiveIntegerField()
    content_obj = GenericForeignKey('content_type', 'content_id')

    # TODO: check if unique inside the scope of one page
    order_idx = models.IntegerField(default=0)

    class Meta:
        ordering = ('order_idx',)
        unique_together = ('page', 'order_idx')
