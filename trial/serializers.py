# coding=utf-8
from rest_framework import serializers

from .models import Page, PageContent
from .models_content import exports_list

from .tasks import task_content_hit_counter


class PageListSerializer(serializers.ModelSerializer):
    detailed_url = serializers.HyperlinkedIdentityField('pages-detail')

    class Meta:
        model = Page
        fields = ('id', 'title', 'detailed_url')


def _create_content_serializers(exports_list):
    """Black magic for creating serializer class map from model list."""

    class_map = dict()
    for cls in exports_list:
        ser_name = f'{cls.__name__}Serializer'

        serializer_class = type(ser_name, (serializers.ModelSerializer,), {
            'Meta': type(f'{ser_name}.Meta', (), {
                'model': cls,
                'fields': '__all__'
            })
        })

        class_map[cls.__name__] = serializer_class

    return class_map


class ContentObjectRelatedField(serializers.RelatedField):
    """A special field for serializing custom content fields."""

    serializer_map = _create_content_serializers(exports_list)

    def to_representation(self, value):
        try:
            serializer_class = self.serializer_map[value.__class__.__name__]
            serializer = serializer_class(value)
            return serializer.data
        except KeyError as exc:
            raise TypeError('Unexpected type of content object') from exc


class PageContentSerializer(serializers.ModelSerializer):
    content_obj = ContentObjectRelatedField(read_only=True)
    content_type = serializers.PrimaryKeyRelatedField(
        source='content_type.model',
        read_only=True
    )

    class Meta:
        model = PageContent
        fields = ('id', 'content_obj', 'order_idx', 'content_type')


class PageDetailSerializer(serializers.ModelSerializer):
    contents = PageContentSerializer(many=True)

    def to_representation(self, value):
        # FIXME: called multiple times (4?) when accessing in API format,
        #   `retrieve` is only called 1 time

        # Gather queries to be executed in background
        pks_to_update = []
        for content in value.contents.all():
            obj = content.content_obj
            pks_to_update.append((obj.__class__.__name__, obj.pk))

        # Create task
        task_content_hit_counter.delay(pks_to_update)

        return super(PageDetailSerializer, self).to_representation(value)

    class Meta:
        model = Page
        fields = ('id', 'title', 'contents')
