# Use an official Python runtime as a parent image
FROM python:3.6-slim as base

WORKDIR /code

# This and consequent pip install goes first to allow Docker cache it
COPY requirements.txt /code/

# Install packets
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Needed for initial install, after this `volumes` will take over
COPY . /code

ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE "justwork_trial.settings"
